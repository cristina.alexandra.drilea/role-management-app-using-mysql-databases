#include <iostream>
#include "HandleOperations.hpp"

int main(void)
{
	int choice;
	HandleOperations ho;
	bool exit = false;
	do {
		std::cout << std::endl
			<< " 1 - Get containers\n"
			<< " 2 - Add container\n"
			<< " 3 - Update container\n"
			<< " 4 - Delete container\n"
			<< " 5 - Get role\n"
			<< " 6 - Add role\n"
			<< " 7 - Update role\n"
			<< " 8 - Delete role\n"
			<< " 9 - Get permission for role\n"
			<< " 10 - Get roles for permission\n"
			<< " 11 - Add a role permission assignment\n"
			<< " 12 - Delete role permission assignment\n"
			<< " 13 - Exit\n"
			<< " Enter your choice: ";
		std::cin >> choice;
		std::cin.get();
		switch (choice) {
		case 1:
			ho.getContainer();
			break;
		case 2:
			ho.addContainerStoredProcedure();
			break;
		case 3:
			ho.updateParentContainer();
			break;
		case 4:
			ho.deleteContainerWithTransaction();   
			break;
		case 5:
			ho.getRole();
			break;
		case 6:
			ho.addRole();
			break;
		case 7:
			ho.updateRole();  //// verificaaa
			break;
		case 8:
			ho.deleteRole();
			break;
		case 9:
			ho.getPermissionsForRole();
			break;
		case 10:
			ho.getRoleForPermission();
			break;
		case 11:
			ho.addPermissionAssignment();
			break;
		case 12:
			ho.deletePermissionAssignment();
			break;
		case 13:
			exit = true;
			break;
		}

	} while (exit != true);


	return 0;
}
