#pragma once



#include "mysql_connection.h"

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>

class HandleOperations
{
	sql::Driver *driver;
	sql::Connection *con;
	sql::Statement *stmt;
	sql::ResultSet *resGetContainer, *resAddContainer;
	sql::PreparedStatement *pstmGetContainer, *pstmAddContainer;
	
public:
	void getContainer();
	void addContainer();
	void addContainerStoredProcedure();
	void updateParentContainer();
	void deleteContainerWithTransaction();
	void getRole();
	void addRole();
	void updateRole();
	void deleteRole();
	void testTransaction();
	void getPermissionsForRole();
	void getRoleForPermission();
	void addPermissionAssignment();
	void deletePermissionAssignment();
	HandleOperations();
	~HandleOperations();
};

